# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#inherit common gapps
$(call inherit-product, vendor/googleapps/common-blobs.mk)

# /app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    FaceLock \
    GoogleCamera \
    GoogleContactsSyncAdapter \
    GoogleDialer \
    GoogleTTS \
    GoogleVrCore \
    NexusWallpapersStubPrebuilt \
    Photos \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    Tycho \
    talkback \
    WallpapersBReel \
    WallpaperPickerGooglePrebuilt \
    WallpapersUsTwo

# /framework
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2016 \
    com.google.android.dialer.support \
    com.google.android.maps \
    com.google.android.media.effects

# /priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    CarrierSetup \
    ConfigUpdater \
    ConnMetrics \
    GCS \
    GmsCoreSetupPrebuilt \
    GoogleBackupTransport \
    GoogleContacts \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    NexusLauncherPrebuilt \
    Phonesky \
    PrebuiltGmsCorePix \
    DynamiteLoader.apk \
    DynamiteModulesA \
    DynamiteModulesB \
    DynamiteModulesC \
    DynamiteModulesD \
    GoogleCertificates \
    SetupWizard \
    StorageManagerGoogle \
    TagsGoogle \
    Turbo \
    Velvet

# Include package overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/googleapps/overlay/

# build.prop entrys
PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.ime.theme_id=5 \
    ro.wallpapers_loc_request_suw=true

# Libraries
PRODUCT_COPY_FILES += \
    vendor/googleapps/lib64/libgdx.so:system/lib64/libgdx.so \
    vendor/googleapps/lib64/libgeswallpapers-jni.so:system/lib64/libgeswallpapers-jni.so

# /symlinks
PRODUCT_PACKAGES += \
    libfacenet.so \
    libgdx.so \
    libgeswallpapers-jni.so \
    libjpeg.so
